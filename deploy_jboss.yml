---
- name: Deploy {{ jboss_eap_display_name }} on Windows
  hosts: '{{ target_hosts }}'

  tasks:
    - name: install jdk package
      win_chocolatey:
        name: '{{ jdk_package }}'
        state: present
        version: '{{ java_major_version }}.{{ java_minor_version }}'

    - name: set JAVA_HOME as facts
      set_fact:
        java_home: '{{ ansible_env.ProgramFiles }}\Java\jdk1.{{ java_major_version }}_{{ java_minor_version }}'

    - name: set JAVA_HOME environment
      win_environment:
        state: present
        name: JAVA_HOME
        value: '{{ java_home }}'
        level: machine

    - name: add java executable to default path
      win_path:
        elements:
          - '{{ ansible_env.ProgramFiles }}\Java\jdk1.{{ java_major_version }}_{{ java_minor_version }}\bin'

    - name: download {{ jboss_eap_name }} zip package
      win_get_url:
        url: '{{ download_base_url }}/{{ jboss_eap_name }}-{{ jboss_eap_version }}.zip'
        dest: '{{ ansible_env.TEMP }}\{{ jboss_eap_name }}-{{ jboss_eap_version }}.zip'

    - name: extract {{ jboss_eap_name }} zip package
      win_unzip:
        src: '{{ ansible_env.TEMP }}\{{ jboss_eap_name }}-{{ jboss_eap_version }}.zip'
        dest: '{{ ansible_env.SystemDrive }}\{{ jboss_root_dir }}'
        creates: '{{ ansible_env.SystemDrive }}\{{ jboss_root_dir }}'

    - name: set JBOSS_HOME as facts
      set_fact:
        jboss_home: '{{ ansible_env.SystemDrive }}\{{ jboss_root_dir }}\{{ jboss_eap_name }}-{{ jboss_eap_version }}'

    - name: set JBOSS_HOME environment
      win_environment:
        state: present
        name: JBOSS_HOME
        value: '{{ jboss_home }}'
        level: machine

    - name: copy files jboss_home JBOSS_HOME
      win_copy:
        src: jboss_home/
        dest: '{{ jboss_home }}\'

    - name: create {{ jboss_eap_name }} user admin
      win_shell: |
        {{ jboss_home }}\bin\add-user.bat -s {{ jboss_eap_admin }} {{ jboss_eap_password | quote }}
      environment:
        JAVA_HOME: '{{ java_home }}'
        JBOSS_HOME: '{{ jboss_home }}'

    - name: set {{ jboss_eap_name }} bind address to public interface
      win_lineinfile:
        path: '{{ jboss_home }}\bin\{{ jboss_eap_mode }}.conf.bat'
        regexp: '^set\s.*jboss\.bind\.address='
        insertbefore: '^:JAVA_OPTS_SET'
        line: 'set "JAVA_OPTS=%JAVA_OPTS% -Djboss.bind.address={{ ansible_host }}"'

    - name: set {{ jboss_eap_name }} management bind address to public interface
      win_lineinfile:
        path: '{{ jboss_home }}\bin\{{ jboss_eap_mode }}.conf.bat'
        regexp: '^set\s.*jboss\.bind\.address\.management='
        insertbefore: '^:JAVA_OPTS_SET'
        line: 'set "JAVA_OPTS=%JAVA_OPTS% -Djboss.bind.address.management={{ ansible_host }}"'

    - name: set {{ jboss_eap_name }} custom java opts
      win_lineinfile:
        path: '{{ jboss_home }}\bin\{{ jboss_eap_mode }}.conf.bat'
        regexp: '^set\s.*spectrum-log4j\.xml'
        insertbefore: '^:JAVA_OPTS_SET'
        line: 'set "JAVA_OPTS=%JAVA_OPTS% -Dlog4j.configuration={{ jboss_home }}\spectrum-log4j.xml -Dspectrum-security=enabled"'

    - name: copy service directory to bin
      win_robocopy:
        src: '{{ jboss_home }}\docs\contrib\scripts\service'
        dest: '{{ jboss_home }}\bin\service'
        recurse: yes
        purge: yes
      when: not jsvc_install|bool

    - name: kill prunsrv.exe
      win_shell: |
        net stop {{ jboss_eap_service_name }}
        taskkill /f /im prunsrv.exe
      environment:
        JAVA_HOME: '{{ java_home }}'
        JBOSS_HOME: '{{ jboss_home }}'
        NOPAUSE: 'true'
      ignore_errors: yes
      when: jsvc_install|bool

    - name: download {{ jsvc_name }} package
      win_get_url:
        url: '{{ jsvc_download_uri }}'
        dest: '{{ ansible_env.TEMP }}\{{ jsvc_name }}-{{ jsvc_version }}.{{ jsvc_arch }}.zip'
      when: jsvc_install|bool

    - name: extract {{ jsvc_name }} package
      win_unzip:
        src: '{{ ansible_env.TEMP }}\{{ jsvc_name }}-{{ jsvc_version }}.{{ jsvc_arch }}.zip'
        dest: '{{ ansible_env.SystemDrive }}\{{ jboss_root_dir }}'
      when: jsvc_install|bool

    - name: install {{ jboss_eap_name }} service
      win_shell: |
        {{ jboss_home }}\bin\{% if not jsvc_install|bool  %}service\{% endif %}service.bat install /config {{ jboss_eap_config }} /name {{ jboss_eap_service_name }} {% if not jsvc_install|bool  %}/display {{ jboss_eap_service_display_name | quote }}{% endif %} /jbossuser {{ jboss_eap_admin }} /jbosspass {{ jboss_eap_password | quote }} /controller {{ ansible_host }}
      environment:
        JAVA_HOME: '{{ java_home }}'
        JBOSS_HOME: '{{ jboss_home }}'
        NOPAUSE: 'true'

    - name: set {{ jboss_eap_name }} service startup mode to auto and ensure it is started
      win_service:
        name: '{{ jboss_eap_service_name }}'
        start_mode: auto
        state: started

    - name: copy jc_run.bat
      win_template:
        src: jc_run.bat.j2
        dest: '{{ jboss_home }}\jc_run.bat'

    - name: execute jc files
      win_shell: |
        {{ jboss_home }}\jc_run.bat
      environment:
        JAVA_HOME: '{{ java_home }}'
        JBOSS_HOME: '{{ jboss_home }}'
        NOPAUSE: 'true'

    - name: set firewall rule to allow default {{ jboss_eap_name }} port (8080, 8443, 9990)
      win_firewall_rule:
        name: '{{ jboss_eap_display_name }}'
        localport: 8080,8443,9990
        action: allow
        direction: in
        protocol: tcp
        state: present
        enabled: yes
