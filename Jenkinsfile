def TF_OPERATION = 'Create'

def getenv(String envname) {
    def result = readProperties file: '.env'
    return result[envname]
}


pipeline {
    agent {
        label "ansible && linux"
    }

    environment {
        ANSIBLE_FORCE_COLOR = getenv("ANSIBLE_NOCOLOR")
        PYTHONUNBUFFERED = getenv("PYTHONUNBUFFERED")
        TERRAFORM_WORKSPACE = getenv("TERRAFORM_WORKSPACE")
        TERRAFORM_VM_COUNT = getenv("TERRAFORM_VM_COUNT")
        TERRAFORM_VM_NAME = getenv("TERRAFORM_VM_NAME")
        TERRAFORM_VM_RG = getenv("TERRAFORM_VM_RG")
    }

    stages {
        stage('Clean Workspace') {
            steps {
                sh "git clean -xdff"
            }
        }

        stage('Install Ansible') {
            steps {
                sh "virtualenv --system-site-packages -p python3 ${WORKSPACE}/ansible-tf-azure"

                withPythonEnv("${WORKSPACE}/ansible-tf-azure/") {
                    sh "pip install --upgrade pip"
                    sh "pip install -r requirements.txt"
                }
            }
        }

        stage('Install Terraform') {
            steps {
                sh "curl -o ${WORKSPACE}/terraform.zip https://releases.hashicorp.com/terraform/0.12.12/terraform_0.12.12_linux_amd64.zip"
                sh "unzip -o -d ${WORKSPACE}/ansible-tf-azure/bin/ ${WORKSPACE}/terraform.zip"
                sh "rm -f ${WORKSPACE}/terraform.zip"
                withCredentials([string(credentialsId: 'ARM_CLIENT_ID', variable: 'ARM_CLIENT_ID'), string(credentialsId: 'ARM_CLIENT_SECRET', variable: 'ARM_CLIENT_SECRET'), string(credentialsId: 'ARM_SUBSCRIPTION_ID', variable: 'ARM_SUBSCRIPTION_ID'), string(credentialsId: 'ARM_TENANT_ID', variable: 'ARM_TENANT_ID')]) {
                    sh "${WORKSPACE}/ansible-tf-azure/bin/terraform init -input=false -no-color"
                }
            }
        }

        stage('Terraform Operation') {
            steps {
                script {
                    if ("${TERRAFORM_VM_COUNT}".toInteger() > 0) {
                        TF_OPERATION = 'Create'
                    } else {
                        TF_OPERATION = 'Delete'
                    }
                }
                echo "Terraform Operation: ${TF_OPERATION}"
            }
        }

        stage('Terraform Destroy') {
            when {
                expression {
                    TF_OPERATION == 'Delete'
                }
            }
            steps {
                withCredentials([string(credentialsId: 'ARM_CLIENT_ID', variable: 'ARM_CLIENT_ID'), string(credentialsId: 'ARM_CLIENT_SECRET', variable: 'ARM_CLIENT_SECRET'), string(credentialsId: 'ARM_SUBSCRIPTION_ID', variable: 'ARM_SUBSCRIPTION_ID'), string(credentialsId: 'ARM_TENANT_ID', variable: 'ARM_TENANT_ID')]) {
                    sh "${WORKSPACE}/ansible-tf-azure/bin/terraform workspace select ${TERRAFORM_WORKSPACE} -no-color"
                    sh "${WORKSPACE}/ansible-tf-azure/bin/terraform plan -input=false -no-color"
                    sh "${WORKSPACE}/ansible-tf-azure/bin/terraform destroy -input=false -no-color -auto-approve"
                }
            }
        }

        stage('Terraform Apply') {
            when {
                expression {
                    TF_OPERATION == 'Create'
                }
            }
            steps {
                withCredentials([string(credentialsId: 'ARM_CLIENT_ID', variable: 'ARM_CLIENT_ID'), string(credentialsId: 'ARM_CLIENT_SECRET', variable: 'ARM_CLIENT_SECRET'), string(credentialsId: 'ARM_SUBSCRIPTION_ID', variable: 'ARM_SUBSCRIPTION_ID'), string(credentialsId: 'ARM_TENANT_ID', variable: 'ARM_TENANT_ID')]) {
                    sh "${WORKSPACE}/ansible-tf-azure/bin/terraform workspace new ${TERRAFORM_WORKSPACE} -no-color || ${WORKSPACE}/ansible-tf-azure/bin/terraform workspace select ${TERRAFORM_WORKSPACE} -no-color"
                    sh "${WORKSPACE}/ansible-tf-azure/bin/terraform plan -input=false -no-color -out t_plan -var count_of_VMs=${TERRAFORM_VM_COUNT} -var VMRG=${TERRAFORM_VM_RG} -var vm_name=${TERRAFORM_VM_NAME}"
                    sh "${WORKSPACE}/ansible-tf-azure/bin/terraform apply -input=false -no-color t_plan"
                }
            }
        }

        stage('Check Connection') {
            when {
                expression {
                    TF_OPERATION == 'Create'
                }
            }
            steps {
                withPythonEnv("${WORKSPACE}/ansible-tf-azure/") {
                    ansiblePlaybook colorized: true,
                                    installation: 'ansible',
                                    credentialsId: 'vignoadmin',
                                    playbook: 'winrm_ping.yml',
                                    extras: '-e target_hosts=all'
                }
            }
        }

        stage('Deploy JBoss EAP') {
            when {
                expression {
                    TF_OPERATION == 'Create'
                }
            }
            steps {
                withPythonEnv("${WORKSPACE}/ansible-tf-azure/") {
                    ansiblePlaybook colorized: true,
                                    installation: 'ansible',
                                    credentialsId: 'vignoadmin',
                                    playbook: 'deploy_jboss.yml',
                                    extras: '-e target_hosts=all'
                }
            }
        }

        stage('Deploy JBoss Apps') {
            when {
                expression {
                    TF_OPERATION == 'Create'
                }
            }
            steps {
                withPythonEnv("${WORKSPACE}/ansible-tf-azure/") {
                    ansiblePlaybook colorized: true,
                                    installation: 'ansible',
                                    credentialsId: 'vignoadmin',
                                    playbook: 'deploy_apps.yml',
                                    extras: '-e target_hosts=all'
                }
            }
        }

        stage('Check Installation') {
            when {
                expression {
                    TF_OPERATION == 'Create'
                }
            }
            steps {
                withPythonEnv("${WORKSPACE}/ansible-tf-azure/") {
                    ansiblePlaybook colorized: true,
                                    installation: 'ansible',
                                    credentialsId: 'vignoadmin',
                                    playbook: 'winrm_url.yml',
                                    extras: '-e target_hosts=all'
                }
                sh 'echo "Ansible Inventory List" && cat inventories/hosts'
            }
        }
    }
}
